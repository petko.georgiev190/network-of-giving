package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;



@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    private static User u1;
    private static User u2;
    private static User u3;

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() {
        u1 = new User("u1","username",18, User.Sex.MALE,"asd","asd");
        u2 =new User("u2","username",18, User.Sex.MALE,"asd","asd");
        u3 = new User("u3","username",18, User.Sex.MALE,"asd","asd");

    }

    @Test
    public void WhenFindAllInvokedWithoutDataAssertThatBodyIsEmpty() {

        Mockito.when(userService.findAll()).thenReturn(Arrays.asList());
        assertThat(userController.getAllUsers().getBody().size(), is(0));
        Mockito.verify(userService, Mockito.times(1)).findAll();
    }

    @Test
    public void WhenFindAllInvokedWithDataAssertThatBodyIsNotEmpty() {

        Mockito.when(userService.findAll()).thenReturn(Arrays.asList(u1, u2));
        assertThat(userController.getAllUsers().getBody().size(), is(2));
        Mockito.verify(userService, Mockito.times(1)).findAll();
    }


    @Test
    public void WhenFindByIdInvokedWithDataAssertThatBodyIsNotEmpty() {

        Mockito.when(userService.findById(1L)).thenReturn(Optional.of(u1));
        ResponseEntity<User> p = userController.getUserById(1L);
        assertThat(p.getBody(), is(u1) );
    }

    @Test
    public void WhenFindByIdInvokedWithoutDataAssertThatBodyIsNotEmpty() {

        Mockito.when(userService.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<User> p = userController.getUserById(1L);
        assertThat(p.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void whenUserUpdateInvokedWithoutUserReturnNotFound() {

        Mockito.when(userService.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<User> p = userController.editUser(1L, u1);
        assertThat(p.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

}