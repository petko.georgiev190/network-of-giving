package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTestIT {

    private static User u1;
    private static User u2;
    private static User u3;
    private static User u4;
    private static User u5;


    private static final int port =8080;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void init() {
        u1 = new User("u1","username",18, User.Sex.MALE,"asd","asd");
        u2 = new User("u2","username",18, User.Sex.MALE,"asd","asd");
        u3 = new User("u3","username",18, User.Sex.MALE,"asd","asd");
        u4 = new User("u4","username",18, User.Sex.MALE,"asd","asd");
        u5 = new User("u5","username",18, User.Sex.MALE,"asd","asd");
    }

    @Test
    public void whenGetAllUsersInvokedExpectOKResponseStatusAndBodyNotNull() {

        ResponseEntity<List<User>> responseEntity = restTemplate.exchange("/users",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {});
        HttpStatus responseStatus = responseEntity.getStatusCode();
        final List<User> userList = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseStatus);
        assertNotNull(userList);
        assertFalse(userList.isEmpty());
    }

    @Test
    public void whenGetAllUsersInvokedExpectOKResponseStatusAndBodyNotNull2() {

        ResponseEntity<User[]> result= this.restTemplate
                .getForEntity("http://localhost:"+port+"/users/", User[].class);

        assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));

        assertThat(result.getBody(), is(notNullValue()));
    }


    @Test
    public void whenCreateUserInvokedWithCorrectDataCreateUserAndReturnOk() {

        HttpEntity<User> request = new HttpEntity<>(u1);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:"+port+"/users/", request, User.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody().getName(), is("u1"));
        restTemplate.delete("http://localhost:"+port+"/users/delete/"+response.getBody().getUserId());
    }


    @Test
    public void whenFindByIdInvokedExpectOKResponseStatusAndBodyNotNull() {

        HttpEntity<User> request = new HttpEntity<>(u4);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:"+port+"/users/", request, User.class);

        long id = response.getBody().getUserId();

        response = restTemplate.getForEntity("http://localhost:"+port+"/users/"+id, User.class);

        assertThat(response.getBody().getUserId(), is(id));
        assertThat(response.getBody().getName(), equalTo(u4.getName()));
        restTemplate.delete("http://localhost:"+port+"/users/delete/"+id);
    }

    @Test
    public void whenDeleteUserInvokedDeleteUserAndAssertNotNull() {

        HttpEntity<User> request = new HttpEntity<>(u5);
        ResponseEntity<User> response = restTemplate.postForEntity("http://localhost:"+port+"/users/", request, User.class);

        long id = response.getBody().getUserId();

        restTemplate.delete("http://localhost:"+port+"/users/delete/"+id);
        response = restTemplate.getForEntity("http://localhost:"+port+"/users/"+id, User.class);

        assertThat(response.getBody(), is(nullValue()));

    }


}