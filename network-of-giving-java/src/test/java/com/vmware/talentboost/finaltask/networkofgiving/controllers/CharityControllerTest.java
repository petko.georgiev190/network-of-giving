package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.CharityService;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.vmware.talentboost.finaltask.networkofgiving.entities.users.User.Sex.MALE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class CharityControllerTest {
    private static Charity c1;
    private static Charity c2;
    private static Charity c3;
    private static User u1;

    @Mock
    private CharityService charityService;
    @Mock
    private UserService userService;
    @Mock
    private User user;

    @InjectMocks
    private CharityController charityController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void init() {
        c1 = new Charity("c1","c1",12,12,user,"");
        c2 =new Charity("c2","c2",12,12,user,"");
        c3 = new Charity("c3","c3",12,12,user,"");
        u1 = new User("name", "user", 17, MALE, "pass", "as");

    }

    @Test
    public void WhenFindAllInvokedWithoutDataAssertThatBodyIsEmpty() {

        Mockito.when(charityService.findAll()).thenReturn(Arrays.asList());
        assertThat(charityController.getAllCharities().getBody().size(), is(0));
        Mockito.verify(charityService, Mockito.times(1)).findAll();
    }

    @Test
    public void WhenFindAllInvokedWithDataAssertThatBodyIsNotEmpty() {

        Mockito.when(charityService.findAll()).thenReturn(Arrays.asList(c1, c2));
        assertThat(charityController.getAllCharities().getBody().size(), is(2));
        Mockito.verify(charityService, Mockito.times(1)).findAll();
    }


    @Test
    public void WhenFindByIdInvokedWithDataAssertThatBodyIsNotEmpty() {

        Mockito.when(charityService.findById(1L)).thenReturn(Optional.of(c1));
        ResponseEntity<Charity> p = charityController.getCharityById(1L);
        assertThat(p.getBody(), is(c1) );
    }

    @Test
    public void WhenFindByIdInvokedWithoutDataAssertThatBodyIsNotEmpty() {

        Mockito.when(charityService.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Charity> p = charityController.getCharityById(1L);
        assertThat(p.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void whenGetCharityByNameMatchesReturnOk(){
        Mockito.when(charityService.findByCharityName("sample")).thenReturn(Optional.of(c1));
        ResponseEntity<Charity> p = charityController.getCharityByName("sample");
        assertThat(p.getBody(), is(c1) );

    }

    @Test
    public void whenGetCharityByNameDoesNotMatchReturnNotFound(){
        Mockito.when(charityService.findByCharityName("sample")).thenReturn(Optional.empty());
        ResponseEntity<Charity> p = charityController.getCharityByName("sample");
        assertThat(p.getStatusCode(), is(HttpStatus.NOT_FOUND));

    }

    @Test
    public void whenSearchForCharityByNameDoesNotMatchReturnNotFound(){
        Mockito.when(charityService.searchByName("sample")).thenReturn(Arrays.asList(c1, c2));
        ResponseEntity<List<Charity>> p = charityController.getCharityBySearching("sample");
        assertThat(p.getBody().size(), is(2) );
        Mockito.verify(charityService, Mockito.times(1)).searchByName("sample");

    }

    @Test
    public void whenGetCharitiesCreatedByUserMatchesSomethingReturnListOfCharitiesAndOk(){
        Mockito.when(charityService.findByCreator(u1)).thenReturn(Arrays.asList(c1, c2));
        Mockito.when(userService.findByUsername("sample")).thenReturn(Optional.of(u1));
        ResponseEntity<List<Charity>> p =charityController.getCharitiesCreatedByUser("sample");
        assertThat(p.getBody().size(),is(2));
        assertThat(p.getStatusCode(), is(HttpStatus.OK));

    }

    @Test
    public void whenGetCharitiesCreatedByUserMatchesNothingReturnNotFound(){
        Mockito.when(userService.findByUsername("sample")).thenReturn(Optional.empty());
        ResponseEntity<List<Charity>> p =charityController.getCharitiesCreatedByUser("sample");
        assertThat(p.getStatusCode(), is(HttpStatus.NOT_FOUND));

    }


}