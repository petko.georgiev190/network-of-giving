package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;


@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CharityControllerTestIT {
    private static final int port =8080;
    private static Charity c1;

    @Mock
    private User user;

    @Autowired
    private TestRestTemplate restTemplate;
    @Before
    public void init() {
        c1 = new Charity("c1","c1",12,12,user,"");
    }

    @Test
    public void whenGetAllCharitiesInvokedExpectOKResponseStatusAndBodyNotNull() {

        ResponseEntity<List<Charity>> responseEntity = restTemplate.exchange("/charities",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Charity>>() {});
        HttpStatus responseStatus = responseEntity.getStatusCode();
        final List<Charity> charityList = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseStatus);
        assertNotNull(charityList);
        assertFalse(charityList.isEmpty());
    }


}
