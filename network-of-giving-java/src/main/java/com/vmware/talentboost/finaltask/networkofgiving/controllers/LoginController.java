package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLogInput;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.UserShell;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveUserDetailsServiceAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class LoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private EventLogInput logInput;

    @PostMapping("/users/authenticate")
    public ResponseEntity<User> authenticateUser( @RequestBody UserShell user){
        Optional<User> userBody = userService.findByUsername(user.getUsername());
        if(!userBody.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User userDetails=userBody.get();
        if(user.getUsername().equals(userDetails.getUsername()) && user.getPassword().equals(userDetails.getPassword())){
            logInput.createLogLogin(userDetails);
            return new ResponseEntity<>(userDetails,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
