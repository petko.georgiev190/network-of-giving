package com.vmware.talentboost.finaltask.networkofgiving.services;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.repositories.CharityRepository;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CharityService {
    @Autowired
    private CharityRepository charityRepository;

    public List<Charity> findAll() {
        return charityRepository.findAll();
    }

    public Optional<Charity> findById(long id) {
        return charityRepository.findById(id);
    }
    public Optional<Charity> findByCharityName(String name) {
        return charityRepository.findByCharityName(name);
    }
    public List<Charity> searchByName(String name){
        return charityRepository.findByCharityNameContaining(name);
    }
    public List<Charity> findByCreator(User username){
        return charityRepository.findByCreator(username);
    }

    public Charity createOrUpdate(Charity charity) {
        return charityRepository.save(charity);
    }

    public void deleteById(long id) {
        charityRepository.deleteById(id);
    }
}
