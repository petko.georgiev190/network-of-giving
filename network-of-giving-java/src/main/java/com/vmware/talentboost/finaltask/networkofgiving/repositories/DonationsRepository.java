package com.vmware.talentboost.finaltask.networkofgiving.repositories;

import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.DonationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DonationsRepository extends JpaRepository<Donation, DonationKey> {

}
