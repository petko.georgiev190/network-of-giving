package com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DonationKey implements Serializable {

    @Column(name = "charity_id")
    private long charityId;

    @Column(name = "user_id")
    private long userId;

    public DonationKey(long charityId, long userId) {
        this.charityId = charityId;
        this.userId = userId;
    }

    public DonationKey(){

    }
    public long getCharityId() {
        return charityId;
    }

    public void setCharityId(long charityId) {
        this.charityId = charityId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DonationKey that = (DonationKey) o;
        return getCharityId() == that.getCharityId() &&
                getUserId() == that.getUserId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCharityId(), getUserId());
    }
}
