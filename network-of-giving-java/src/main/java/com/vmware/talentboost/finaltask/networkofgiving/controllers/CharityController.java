package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.DonationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.ParticipationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLog;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLogInput;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@CrossOrigin(origins="http://localhost:4200")
@RestController
public class CharityController {
    @Autowired
    private CharityService charityService;

    @Autowired
    private UserService userService;
    @Autowired
    private DonationService donationService;

    @Autowired
    private ParticipationService participationService;
    @Autowired
    private EventLogInput logInput;
    @Autowired
    private EventLoggerService eventService;

    @GetMapping("/charities")
    public ResponseEntity<List<Charity>> getAllCharities() {
        return new ResponseEntity<List<Charity>>(charityService.findAll(),HttpStatus.OK);
    }

    @GetMapping("/charities/getById/{id}")
    public ResponseEntity<Charity> getCharityById(@PathVariable long id){
        Optional<Charity> charityData= charityService.findById(id);
        return getCharityResponseEntity(charityData);

    }

    @GetMapping("/charities/getByName/{charityName}")
    public ResponseEntity<Charity> getCharityByName(@PathVariable(value = "charityName") String charityName) {
        Optional<Charity> charityData = charityService.findByCharityName(charityName);
        return getCharityResponseEntity(charityData);
    }

    @GetMapping("/charities/searchByName/{charityName}")
    public ResponseEntity<List<Charity>> getCharityBySearching(@PathVariable(value = "charityName")String charityName){
        return new ResponseEntity<>(charityService.searchByName(charityName),HttpStatus.OK);
    }

    @GetMapping("/charities/getByCreator/{username}")
    public ResponseEntity<List<Charity>> getCharitiesCreatedByUser(@PathVariable String username) {
        Optional<User> userData = userService.findByUsername(username);
        if(userData.isPresent()){
            User user = userData.get();
            return new ResponseEntity<List<Charity>>(charityService.findByCreator(user), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/users/{username}/charities/create")
    public ResponseEntity<Charity> createCharity (@PathVariable String username,@RequestBody Charity charity){

        Optional<User> userData = userService.findByUsername(username);
        User user;
        if(userData.isPresent()){
            user = userData.get();
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        if(charityService.findByCharityName(charity.getCharityName()).isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Charity charityCreated = charityService.createOrUpdate(
                new Charity(charity.getCharityName(),charity.getDescription(),charity.getNumberOfParticipantsWanted(),charity.getBudgetRequired(),user,charity.getThumbnailUrl()));


        logInput.createLogCharityCreation(user,charityCreated);

        return new ResponseEntity<>(charityCreated,HttpStatus.OK);
        }

    @PutMapping("/users/{username}/charities/{id}")
    public ResponseEntity<Charity> updateCharity(
            @PathVariable String username,
            @PathVariable long id, @RequestBody Charity charityDetails){

        Optional<User> userData = userService.findByUsername(username);
        User user;
        if(userData.isPresent()){
            user = userData.get();
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Optional<Charity> charityOpt = charityService.findById(id);
        Charity charity;
        if(charityOpt.isPresent()){
            charity = charityOpt.get();
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


        if(charity.getNumberOfParticipants()>charityDetails.getNumberOfParticipantsWanted()){
            charityDetails.setNumberOfParticipants(charity.getNumberOfParticipants());
            List<EventLog> events = eventService.findByEventAndCharity(charity, EventLog.Event.participate);
            for (int i = 0; i < events.size(); i++) {
                if(charityDetails.getNumberOfParticipants()==charityDetails.getNumberOfParticipantsWanted()){
                    break;
                }
                EventLog log =events.get(i);
                ParticipationKey key =new ParticipationKey(log.getCharity().getCharityId(),log.getUser().getUserId());
                if(participationService.findById(key).isPresent()){
                    logInput.createLogParticipationReverted(participationService.findById(key).get().getUser(),charity);
                    participationService.deleteById(key);
                    charityDetails.setNumberOfParticipants(charityDetails.getNumberOfParticipants()-1);
                }
            }
        }


        if(charity.getBudgetCollected()>charityDetails.getBudgetRequired()){
            
            List<EventLog> events = eventService.findByEventAndCharity(charity, EventLog.Event.donate);
            double percentDifference = charityDetails.getBudgetRequired()/charity.getBudgetCollected();
            System.out.println(percentDifference);
            for(int i = 0; i < events.size(); i++) {
                DonationKey key =new DonationKey(events.get(i).getCharity().getCharityId(),events.get(i).getUser().getUserId());
                if(donationService.findById(key).isPresent()){

                    Donation donation =donationService.findById(key).get();

                    double moneyRemained= events.get(i).getAmountDonated()*percentDifference;
                    double moneyReturned =events.get(i).getAmountDonated()-moneyRemained;
                    logInput.createLogMoneyReverted(donation.getUser(),charity,moneyReturned);


                    donation.setAmountDonated(moneyRemained);
                    donationService.createOrUpdate(donation);
                }
            }
            charity.setBudgetCollected(charityDetails.getBudgetRequired());
        }


        charity.setNumberOfParticipants(charityDetails.getNumberOfParticipants());
        charity.setBudgetRequired(charityDetails.getBudgetRequired());
        charity.setCharityId(id);
        charity.setDescription(charityDetails.getDescription());
        charity.setCharityName(charityDetails.getCharityName());
        charity.setNumberOfParticipantsWanted(charityDetails.getNumberOfParticipantsWanted());
        charity.setThumbnailUrl(charityDetails.getThumbnailUrl());
        charity.setEstimatedAmount(charityDetails.getEstimatedAmount());



        charity.setCreator(user);
        Charity charityUpdate = charityService.createOrUpdate(charity);
        return new ResponseEntity<Charity>(charityUpdate, HttpStatus.OK);
    }



    private void removeParticipators(@RequestBody Charity charityDetails, Charity charity) {
        if(charity.getNumberOfParticipants()>charityDetails.getNumberOfParticipantsWanted()){
            List<EventLog> events = eventService.findByEventAndCharity(charity, EventLog.Event.participate);
            for (int i = 0; i < events.size(); i++) {
                if(charityDetails.getNumberOfParticipants()==charityDetails.getNumberOfParticipantsWanted()){
                    break;
                }
                EventLog log =events.get(i);
                ParticipationKey key =new ParticipationKey(log.getCharity().getCharityId(),log.getUser().getUserId());
                if(participationService.findById(key).isPresent()){
                    participationService.deleteById(key);
                    charityDetails.setNumberOfParticipants(charityDetails.getNumberOfParticipants()-1);
                }
            }
        }
    }


    @DeleteMapping("/charities/delete/{userId}/{charityId}")
    public ResponseEntity<Void> deleteCharity(
            @PathVariable long charityId,@PathVariable long userId){
            Optional<Charity> charityData = charityService.findById(charityId);
            if(!charityData.isPresent()){
                return ResponseEntity.noContent().build();
            }
            Charity charity = charityData.get();
            Optional<User> userData = userService.findById(userId);
            if(userData.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            User user = userData.get();
            if(!user.equals(charity.getCreator())){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        for (EventLog event : eventService.findByCharity(charity)) {
            event.setCharity(null);
            eventService.createOrUpdate(event);
        }

        logInput.createLogCharityDeletion(user,charity.getCharityName());
        charityService.deleteById(charityId);

        return ResponseEntity.ok().build();
    }

    private ResponseEntity<Charity> getCharityResponseEntity(Optional<Charity> charityData) {
        if (charityData.isPresent()) {
            return new ResponseEntity<Charity>(charityData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/participation/from/{userId}/to")
    public ResponseEntity<List<Charity>> findCharitiesByUserParticipated(@PathVariable long userId) {
        Optional<User> userData = userService.findById(userId);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userData.get();
        List<Participation> participation = participationService.findAll();
        List<Charity> charities = new ArrayList<>();
        for (int i = 0; i < participation.size(); i++) {
            if (participation.get(i).getId().getUserId() == user.getUserId()) {
                Charity curr = charityService.findById(participation.get(i).getId().getCharityId()).get();
                charities.add(curr);


            }
        }
        return new ResponseEntity<>(charities,HttpStatus.OK);
    }

}
