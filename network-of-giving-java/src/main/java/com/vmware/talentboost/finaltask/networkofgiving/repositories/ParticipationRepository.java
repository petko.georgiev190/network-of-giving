package com.vmware.talentboost.finaltask.networkofgiving.repositories;

import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.ParticipationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipationRepository extends JpaRepository<Participation, ParticipationKey> {
}
