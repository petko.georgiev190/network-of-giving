package com.vmware.talentboost.finaltask.networkofgiving.entities.charities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLog;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.EventLoggerService;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="charities")
@JsonIgnoreProperties("charitiesDonatedTo")
public class Charity {


    @Id
    @GeneratedValue
    @Column(name = "charity_id")
    private long charityId;

    @Column(nullable = false, unique = true, name = "charity_name")
    private String charityName;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "number_of_participants")
    private int numberOfParticipants;

    @Column(name = "number_of_participants_wanted")
    private int numberOfParticipantsWanted;

    @Column(name = "thumbnail_url")
    private String thumbnailUrl;

    @Column(name = "is_done")
    private boolean isDone;

    @Column(name = "budget_required")
    private double budgetRequired;

    @Column(name = "budget_collected")
    private double budgetCollected;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @Column(name = "estimated_amount")
    private double estimatedAmount;

    @Column(name = "budget_met")
    private boolean budgetMet;

    @Column(name = "number_of_participants_met")
    private boolean numberOfParticipantsMet;



    @OneToMany(mappedBy = "charity")
    @OnDelete(action = OnDeleteAction.CASCADE)
    Set<Donation> donationFrom;

    @OneToMany(mappedBy = "charity")
    @OnDelete(action = OnDeleteAction.CASCADE)
    Set<Participation> participationBy;


    public Charity(String charityName, String description, int numberOfParticipantsWanted, double budgetRequired, User creator,String thumbnailUrl) {

        this.charityName = charityName;
        this.description = description;
        this.numberOfParticipantsWanted = numberOfParticipantsWanted;
        this.budgetRequired = budgetRequired;
        this.creator = creator;
        this.isDone = false;
        this.thumbnailUrl=thumbnailUrl;
        estimatedAmount = 0;
        budgetMet = false;
        numberOfParticipantsMet = false;
        numberOfParticipants = 0;
        budgetCollected = 0;
    }

    public Charity() {

    }

    public long getCharityId() {
        return charityId;
    }

    public void setCharityId(long charityId) {
        this.charityId = charityId;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(int numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public int getNumberOfParticipantsWanted() {
        return numberOfParticipantsWanted;
    }

    public void setNumberOfParticipantsWanted(int numberOfParticipantsWanted) {
        this.numberOfParticipantsWanted = numberOfParticipantsWanted;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public double getBudgetRequired() {
        return budgetRequired;
    }

    public void setBudgetRequired(double budgetRequired) {
        this.budgetRequired = budgetRequired;
    }

    public double getBudgetCollected() {
        return budgetCollected;
    }

    public void setBudgetCollected(double budgetCollected) {
        this.budgetCollected = budgetCollected;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public double getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(double estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public boolean isBudgetMet() {
        return budgetMet;
    }

    public void setBudgetMet(boolean budgetMet) {
        this.budgetMet = budgetMet;
    }

    public boolean isNumberOfParticipantsMet() {
        return numberOfParticipantsMet;
    }

    public void setNumberOfParticipantsMet(boolean numberOfParticipantsMet) {
        this.numberOfParticipantsMet = numberOfParticipantsMet;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Charity charity = (Charity) o;
        return getCharityId() == charity.getCharityId() &&
                getNumberOfParticipants() == charity.getNumberOfParticipants() &&
                getNumberOfParticipantsWanted() == charity.getNumberOfParticipantsWanted() &&
                isDone() == charity.isDone() &&
                getBudgetRequired() == charity.getBudgetRequired() &&
                getBudgetCollected() == charity.getBudgetCollected() &&
                getEstimatedAmount() == charity.getEstimatedAmount() &&
                isBudgetMet() == charity.isBudgetMet() &&
                isNumberOfParticipantsMet() == charity.isNumberOfParticipantsMet() &&
                getCharityName().equals(charity.getCharityName()) &&
                getDescription().equals(charity.getDescription()) &&
                getCreator().equals(charity.getCreator()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCharityId(), getCharityName(), getDescription(), getNumberOfParticipants(), getNumberOfParticipantsWanted(), isDone(), getBudgetRequired(), getBudgetCollected(), getCreator(), getEstimatedAmount(), isBudgetMet(), isNumberOfParticipantsMet());
    }
}