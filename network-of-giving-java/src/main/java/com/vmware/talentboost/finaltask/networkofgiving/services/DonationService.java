package com.vmware.talentboost.finaltask.networkofgiving.services;

import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.DonationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import com.vmware.talentboost.finaltask.networkofgiving.repositories.DonationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DonationService {
    @Autowired
    private DonationsRepository donationsRepository;

    public List<Donation> findAll() {
        return donationsRepository.findAll();
    }

    public Optional<Donation> findById(DonationKey id) {
        return donationsRepository.findById(id);
    }


    public Donation createOrUpdate(Donation donation) {
        return donationsRepository.save(donation);
    }

    public void deleteById(DonationKey id) {
        donationsRepository.deleteById(id);
    }
}
