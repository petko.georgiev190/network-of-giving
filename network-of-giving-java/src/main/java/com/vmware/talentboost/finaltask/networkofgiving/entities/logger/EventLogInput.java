package com.vmware.talentboost.finaltask.networkofgiving.entities.logger;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.EventLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
@Service
public class EventLogInput {

    @Autowired
    private EventLoggerService eventService;
    public EventLog createLogCharityCreation(User user, Charity charity){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " created " + charity.getCharityName();
        EventLog event = new EventLog
                (user,charity,0, EventLog.Event.createCharity, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogCharityDeletion(User user, String charityName){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " deleted " + charityName;
        EventLog event = new EventLog
                (user,null,0, EventLog.Event.deleteCharity, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogCharityParticipation(User user, Charity charity){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " volunteered for " + charity.getCharityName();
        EventLog event = new EventLog
                (user,charity,0, EventLog.Event.participate, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);

    }

    public EventLog createLogParticipationReverted(User user, Charity charity) {
        long milis = System.currentTimeMillis();
        String activityString = user.getUsername() + " participation in " + charity.getCharityName() + "reverted!";
        EventLog event = new EventLog
                (user, charity, 0, EventLog.Event.participationReverted, activityString, new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogMoneyReverted(User user, Charity charity,double amount) {
        long milis = System.currentTimeMillis();
        String activityString = amount+ " returned to "+user.getUsername() + " from donation to " + charity.getCharityName();
        EventLog event = new EventLog
                (user, charity, 0, EventLog.Event.moneyReturned, activityString, new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogCharityDonation(User user, Charity charity, double amount){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " donated "+ amount + " to " + charity.getCharityName();
        EventLog event = new EventLog
                (user,charity,amount, EventLog.Event.donate, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }
    public EventLog createLogCharityBudgetMet(User user, Charity charity){
        long milis=System.currentTimeMillis();
        String activityString = charity.getCharityName() + " met budged! ";
        EventLog event = new EventLog
                (user,charity,0, EventLog.Event.charityCollectedMoney, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogCharityParticipantsMet(User user, Charity charity){
        long milis=System.currentTimeMillis();
        String activityString = charity.getCharityName() + " met participants! ";
        EventLog event = new EventLog
                (user,charity,0, EventLog.Event.charityMetParticipation, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogRegister(User user){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " is now registered! ";
        EventLog event = new EventLog
                (user,null,0, EventLog.Event.register, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

    public EventLog createLogLogin(User user){
        long milis=System.currentTimeMillis();
        String activityString = user.getUsername() + " is now logged in! ";
        EventLog event = new EventLog
                (user,null,0, EventLog.Event.login, activityString,new Timestamp(milis));
        return eventService.createOrUpdate(event);
    }

}
