package com.vmware.talentboost.finaltask.networkofgiving.repositories;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CharityRepository extends JpaRepository<Charity, Long> {
    Optional<Charity> findByCharityName(@Param("charityName") String charityName);
    List<Charity> findByCreator(@Param("username") User username);
    List<Charity> findByCharityNameContaining(@Param("charityName") String charityName);

};

