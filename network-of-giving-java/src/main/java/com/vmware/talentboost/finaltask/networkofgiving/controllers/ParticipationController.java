package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.ParticipationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLogInput;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.CharityService;
import com.vmware.talentboost.finaltask.networkofgiving.services.ParticipationService;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class ParticipationController {
    @Autowired
    private UserService userService;
    @Autowired
    private CharityService charityService;
    @Autowired
    private ParticipationService participationService;
    @Autowired
    private EventLogInput logInput;

    @GetMapping("/participation")
    public ResponseEntity<List<Participation>> getAllParticipation() {
        return new ResponseEntity<List<Participation>>(participationService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/participation/from/{userId}")
    public ResponseEntity<List<Participation>> findParticipationByUser(@PathVariable long userId) {
        Optional<User> userData = userService.findById(userId);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userData.get();
        List<Participation> participation = participationService.findAll();
        participation.removeIf(i -> (!i.getUser().equals(user)));

        return new ResponseEntity<List<Participation>>(participation, HttpStatus.OK);

    }

    @GetMapping("/participation/in/{charityId}")
    public ResponseEntity<List<Participation>> findParticipationByCharity(@PathVariable long charityId) {
        Optional<Charity> charityData = charityService.findById(charityId);

        if (!charityData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Charity charity = charityData.get();
        List<Participation> participation = participationService.findAll();
        participation.removeIf(i -> (!i.getCharity().equals(charity)));

        return new ResponseEntity<List<Participation>>(participation, HttpStatus.OK);

    }

    @PostMapping("/users/{userId}/participatesIn/{charityId}")
    public ResponseEntity<Participation> createParticipation(@PathVariable long userId, @PathVariable long charityId,Participation participationData) {
        Optional<User> userData = userService.findById(userId);
        Optional<Charity> charityData = charityService.findById(charityId);

        if (checkIfEntitiesArePresent(userData, charityData)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userData.get();
        Charity charity = charityData.get();
        if(charity.getNumberOfParticipants()==charity.getNumberOfParticipantsWanted()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(participationService.findById(new ParticipationKey(charityId,userId)).isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Participation participation = new Participation();
        participation.setUser(user);
        participation.setCharity(charity);
        participation.setId(new ParticipationKey(charityId,userId));

        logInput.createLogCharityParticipation(user,charity);

        Participation participationCreated = participationService.createOrUpdate(participation);
        charity.setNumberOfParticipants(charity.getNumberOfParticipants()+1);

        if(charity.getNumberOfParticipantsWanted()!=0 && charity.getNumberOfParticipantsWanted()==charity.getNumberOfParticipants()){
            logInput.createLogCharityParticipantsMet(user,charity);
        }

        charityService.createOrUpdate(charity);
        return new ResponseEntity<>(participationCreated, HttpStatus.OK);
    }

    private boolean checkIfEntitiesArePresent(Optional<User> userData, Optional<Charity> charityData) {
        if (!userData.isPresent()) {
            return true;
        }


        if (!charityData.isPresent()) {
            return true;
        }
        return false;
    }
}
