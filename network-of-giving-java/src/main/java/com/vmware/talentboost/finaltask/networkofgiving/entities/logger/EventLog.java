package com.vmware.talentboost.finaltask.networkofgiving.entities.logger;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="logger")
public class EventLog {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "activity_id")
    private long eventId;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "charity_id")
    Charity charity;
    @Column(name="amount_donated")
    double amountDonated;
    @Column(name="activity")
    Event event;
    @Column(name="activityString")
    String eventString;
    @Column(name="time_stamp")
    Timestamp timeStamp;

    public EventLog(User user, Charity charity, double amountDonated, Event event, String eventString, Timestamp timeStamp) {
        this.user = user;
        this.charity = charity;
        this.amountDonated = amountDonated;
        this.event = event;
        this.eventString = eventString;
        this.timeStamp = timeStamp;
    }
    public EventLog(){

    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public double getAmountDonated() {
        return amountDonated;
    }

    public void setAmountDonated(double amountDonated) {
        this.amountDonated = amountDonated;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getEventString() {
        return eventString;
    }

    public void setEventString(String eventString) {
        this.eventString = eventString;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public enum Event {

        createCharity,
        deleteCharity,
        participate,
        donate,
        register,
        login,
        charityCollectedMoney,
        charityMetParticipation,
        participationReverted,
        moneyReturned,

    }
}


