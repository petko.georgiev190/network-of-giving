package com.vmware.talentboost.finaltask.networkofgiving.services;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLog;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.repositories.EventLoggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class EventLoggerService {
    @Autowired
    private EventLoggerRepository eventLoggerRepository;

    public List<EventLog> findAll() {
        return eventLoggerRepository.findAll();
    }

    public List<EventLog> findByEvent(EventLog.Event event) {
        return eventLoggerRepository.findByEventOrderByTimeStampDesc(event);
    }

    public Optional<EventLog> findById(long id) {
        return eventLoggerRepository.findById(id);
    }


    public EventLog createOrUpdate(EventLog logger) {
        return eventLoggerRepository.save(logger);
    }

    public List<EventLog> findByEventAndCharity(Charity charity,EventLog.Event event) {
        return eventLoggerRepository.findByCharityAndEventOrderByTimeStampDesc(charity, event);
    }

    public void deleteById(long id) {
        eventLoggerRepository.deleteById(id);
    }

    public List<EventLog> findByUser(User user){
        return eventLoggerRepository.findByUserOrderByTimeStampDesc(user);
    }

    public List<EventLog> findByCharity(Charity charity){
        return eventLoggerRepository.findByCharityOrderByTimeStampDesc(charity);
    }

}
