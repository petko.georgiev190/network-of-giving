package com.vmware.talentboost.finaltask.networkofgiving.repositories;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLog;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.OrderBy;
import java.util.List;

public interface EventLoggerRepository extends JpaRepository<EventLog, Long> {
    List<EventLog> findByEventOrderByTimeStampDesc(@Param("event") EventLog.Event event);
    List<EventLog> findByUserOrderByTimeStampDesc(@Param("username") User username);
    List<EventLog> findByCharityOrderByTimeStampDesc(@Param("charityName") Charity charity);
    List<EventLog> findByCharityAndEventOrderByTimeStampDesc(@Param("charityName")Charity charity,@Param("event") EventLog.Event event);
}
