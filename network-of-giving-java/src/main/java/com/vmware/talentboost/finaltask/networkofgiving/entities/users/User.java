package com.vmware.talentboost.finaltask.networkofgiving.entities.users;



//import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name="users")
@JsonIgnoreProperties("donationsFrom")
public class User {

    @Column(nullable = false,name="name")
    private String name;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_id")
    private long userId;
    @Column(nullable = false,unique = true,name ="username")
    private String username;
    @Column(nullable = false,name="age")
    private int age;
    @Column(name="sex")
    private Sex sex;
    @Column(nullable = false,name="password")
    private String password;
    @Column(name="location")
    private String location;
    @Column(name="money")
    private int money;

    @OneToMany(mappedBy = "user")
    Set<Donation> charitiesDonatedTo;

    @OneToMany(mappedBy = "user")
    Set<Participation> participatedIn;


   public User(){

    }
    public User(String name, String username, int age, Sex sex, String password, String location) {
        this.name = name;
        this.username = username;
        this.age = age;
        this.sex = sex;
        this.password = password;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getUserId() == user.getUserId() &&
                getAge() == user.getAge() &&
                getMoney() == user.getMoney() &&
                getName().equals(user.getName()) &&
                getUsername().equals(user.getUsername()) &&
                getSex() == user.getSex() &&
                getPassword().equals(user.getPassword()) &&
                getLocation().equals(user.getLocation()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getUserId(), getUsername(), getAge(), getSex(), getPassword(), getLocation(), getMoney());
    }

    public enum Sex{
        MALE,
        FEMALE,
        OTHER
    }




}
