package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLog;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.CharityService;
import com.vmware.talentboost.finaltask.networkofgiving.services.EventLoggerService;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class LoggerController {

    @Autowired
    private EventLoggerService eventLoggerService;

    @Autowired
    private UserService userService;

    @Autowired
    private CharityService charityService;

    @GetMapping("/logger")
    public List<EventLog> getEvents(){
        return eventLoggerService.findAll();
    }
    @GetMapping("/logger/{username}")
    public ResponseEntity<List<EventLog>> getEventsByUser(@PathVariable String username) {
        Optional<User> userData =userService.findByUsername(username);
        if(!userData.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user =userData.get();

        return new ResponseEntity<>(eventLoggerService.findByUser(user),HttpStatus.OK);
    }
    @GetMapping("/logger/byCharity/{charityId}")
    public ResponseEntity<List<EventLog>> getEventsByCharity(@PathVariable long charityId) {
        Optional<Charity> charityData =charityService.findById(charityId);
        if(!charityData.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Charity charity =charityData.get();

        return new ResponseEntity<>(eventLoggerService.findByCharity(charity),HttpStatus.OK);
    }

}
