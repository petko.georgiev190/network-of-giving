package com.vmware.talentboost.finaltask.networkofgiving.entities.participations;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.ParticipationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Participation {
    @EmbeddedId
    ParticipationKey id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user_id")
    private User user;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("charity_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "charity_id")
    private Charity charity;

    public Participation(ParticipationKey id, User user, Charity charity) {
        this.id = id;
        this.user = user;
        this.charity = charity;
    }
    public Participation(){

    }

    public ParticipationKey getId() {
        return id;
    }

    public void setId(ParticipationKey id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participation that = (Participation) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getCharity(), that.getCharity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getCharity());
    }
}
