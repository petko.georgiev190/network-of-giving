package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.DonationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.donations.Donation;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLogInput;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.CharityService;
import com.vmware.talentboost.finaltask.networkofgiving.services.DonationService;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class DonationsController {

    @Autowired
    private CharityService charityService;
    @Autowired
    private DonationService donationService;
    @Autowired
    private UserService userService;
    @Autowired
    private EventLogInput logInput;

    @GetMapping("/donations")
    public ResponseEntity<List<Donation>> getAllDonations() {
        return new ResponseEntity<List<Donation>>(donationService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/donations/from/{userId}/to/{charityId}")
    public Optional<Donation> findDonation(@PathVariable long userId, @PathVariable long charityId) {
        DonationKey key = new DonationKey(charityId, userId);
        Optional<Donation> donationData = donationService.findById(key);
        if (!donationData.isPresent()) {
            return null;
        } else return donationData;
    }

    @GetMapping("/donations/from/{userId}")
    public ResponseEntity<List<Donation>> findDonationsByUser(@PathVariable long userId) {
        Optional<User> userData = userService.findById(userId);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userData.get();
        List<Donation> donations = donationService.findAll();
        donations.removeIf(donation -> (!donation.getUser().equals(user)));

        return new ResponseEntity<List<Donation>>(donations, HttpStatus.OK);

    }

    @GetMapping("/donations/from/{userId}/to")
    public ResponseEntity<List<Charity>> findCharitiesByUserDonated(@PathVariable long userId) {
        Optional<User> userData = userService.findById(userId);

        if (!userData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userData.get();
        List<Donation> donations = donationService.findAll();
        List<Charity> charities = new ArrayList<>();
        for (int i = 0; i < donations.size(); i++) {
            if(donations.get(i).getId().getUserId()==user.getUserId()){
                Charity curr =charityService.findById(donations.get(i).getId().getCharityId()).get();
                charities.add(curr);


            }
        }

        return new ResponseEntity<List<Charity>>(charities, HttpStatus.OK);

    }

    @GetMapping("/donations/to/{charityId}")
    public ResponseEntity<List<Donation>> findDonationsByCharity(@PathVariable long charityId) {
        Optional<Charity> charityData = charityService.findById(charityId);

        if (!charityData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Charity charity = charityData.get();
        List<Donation> donations = donationService.findAll();
        donations.removeIf(donation -> (!donation.getCharity().equals(charity)));

        return new ResponseEntity<List<Donation>>(donations, HttpStatus.OK);

    }

    @PutMapping("/users/{userId}/donatesTo/{charityId}/donate/{amountDonated}")
    public ResponseEntity<Donation> createDonation(@PathVariable long userId, @PathVariable long charityId, @PathVariable double amountDonated) {
        Optional<User> userData = userService.findById(userId);
        Optional<Charity> charityData = charityService.findById(charityId);

        if (checkIfEntitiesArePresent(userData, charityData)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userData.get();
        Charity charity = charityData.get();
        Optional<Donation> presentDonation = findDonation(userId, charityId);
        Donation donation =new Donation(new DonationKey(charityId, userId),user,charity,amountDonated);
        if(!checkIfAmountIsCorrect(charity,donation.getAmountDonated())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        charity.setBudgetCollected(charity.getBudgetCollected()+donation.getAmountDonated());
        if (presentDonation!=null) {
            Donation donationFound = presentDonation.get();
            donationFound.setAmountDonated(donationFound.getAmountDonated() + donation.getAmountDonated());
            donationService.createOrUpdate(donationFound);
            logInput.createLogCharityDonation(user,charity,amountDonated);
            checkForBudgetMetLog(user, charity);
            return new ResponseEntity<Donation>(donationFound, HttpStatus.OK);
        }

        donation.setUser(user);
        donation.setCharity(charity);
        donation.getId().setCharityId(charity.getCharityId());
        donation.getId().setUserId(user.getUserId());
        Donation donationCreated = donationService.createOrUpdate(donation);
        logInput.createLogCharityDonation(user,charity,amountDonated);
        checkForBudgetMetLog(user, charity);
        return new ResponseEntity<Donation>(donationCreated, HttpStatus.OK);
    }

    private void checkForBudgetMetLog(User user, Charity charity) {
        if (charity.getBudgetRequired() != 0 && charity.getBudgetRequired() == charity.getBudgetCollected()) {
            logInput.createLogCharityBudgetMet(user, charity);
        }
    }

    private boolean checkIfAmountIsCorrect(Charity charity, double amountDonated) {
        if(amountDonated<=0 || charity.getBudgetCollected()+amountDonated>charity.getBudgetRequired()){
            return false;
        }
        return true;
    }

    private boolean checkIfEntitiesArePresent(Optional<User> userData, Optional<Charity> charityData) {
        if (!userData.isPresent()) {
            return true;
        }


        if (!charityData.isPresent()) {
            return true;
        }
        return false;
    }
}
