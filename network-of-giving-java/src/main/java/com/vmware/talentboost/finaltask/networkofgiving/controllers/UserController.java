package com.vmware.talentboost.finaltask.networkofgiving.controllers;

import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.logger.EventLogInput;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import com.vmware.talentboost.finaltask.networkofgiving.services.CharityService;
import com.vmware.talentboost.finaltask.networkofgiving.services.EventLoggerService;
import com.vmware.talentboost.finaltask.networkofgiving.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class UserController {

    @Autowired
    private CharityService charityService;

    @Autowired
    private UserService userService;

    @Autowired
    private EventLogInput eventInput;


    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable long userId)  {
        Optional<User> userData = userService.findById(userId);
        return getUserResponseEntity(userData);
    }



    @GetMapping("/users/byCharity/{charityId}")
    public User getCreatorByCharity(@PathVariable long charityId){
        Optional<Charity> charityData = charityService.findById(charityId);
        if(!charityData.isPresent()){
            return null;
        }
        Charity charity =charityData.get();
        User user =charity.getCreator();
        return user;
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> editUser(@PathVariable long id,
            @RequestBody User userDetails){
        Optional<User> userData = userService.findById(id);
        if(!userData.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user =userData.get();
        user=userDetails;
        User userCreated = userService.createOrUpdate(user);

        return new ResponseEntity<>(userCreated,HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(
            @RequestBody User user){
        Optional<User> userData = userService.findByUsername(user.getUsername());
        if(userData.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User userCreated = userService.createOrUpdate(user);
        eventInput.createLogRegister(userCreated);

        return new ResponseEntity<>(userCreated,HttpStatus.OK);
    }


    @DeleteMapping("/users/delete/{id}")
    public ResponseEntity deleteById(@PathVariable long id) {
        Optional<User> p = userService.findById(id);

        if (!p.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        userService.deleteById(id);
        return ResponseEntity.ok().build();
    }


    private ResponseEntity<User> getUserResponseEntity(Optional<User> userData) {
        if(userData.isPresent()){
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
