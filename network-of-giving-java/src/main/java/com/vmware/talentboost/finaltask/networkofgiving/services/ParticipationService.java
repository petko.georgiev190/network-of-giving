package com.vmware.talentboost.finaltask.networkofgiving.services;

import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.ParticipationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.participations.Participation;
import com.vmware.talentboost.finaltask.networkofgiving.repositories.ParticipationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipationService {
    @Autowired
    private ParticipationRepository participationRepository;

    public List<Participation> findAll() {
        return participationRepository.findAll();
    }

    public Optional<Participation> findById(ParticipationKey id) {
        return participationRepository.findById(id);
    }


    public Participation createOrUpdate(Participation participation) {
        return participationRepository.save(participation);
    }

    public void deleteById(ParticipationKey id) {
        participationRepository.deleteById(id);
    }
}
