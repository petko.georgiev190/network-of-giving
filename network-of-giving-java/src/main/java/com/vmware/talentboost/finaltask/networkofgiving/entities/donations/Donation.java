package com.vmware.talentboost.finaltask.networkofgiving.entities.donations;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vmware.talentboost.finaltask.networkofgiving.entities.charities.Charity;
import com.vmware.talentboost.finaltask.networkofgiving.entities.compositekeys.DonationKey;
import com.vmware.talentboost.finaltask.networkofgiving.entities.users.User;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;


@Entity
public class Donation {

    @EmbeddedId
    private DonationKey id;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user_id")
    private User user;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("charity_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "charity_id")
    private Charity charity;
    @Column(name = "amount_donated", nullable = false)
    private double amountDonated;

    public Donation(DonationKey id, User user, Charity charity, double amountDonated) {
        this.id = id;
        this.user = user;
        this.charity = charity;
        this.amountDonated = amountDonated;
    }

    public Donation(){

    }

    public DonationKey getId() {
        return id;
    }

    public void setId(DonationKey id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public double getAmountDonated() {
        return amountDonated;
    }

    public void setAmountDonated(double amountDonated) {
        this.amountDonated = amountDonated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Donation donation = (Donation) o;
        return getAmountDonated() == donation.getAmountDonated() &&
                getId().equals(donation.getId()) &&
                getUser().equals(donation.getUser()) &&
                getCharity().equals(donation.getCharity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getCharity(), getAmountDonated());
    }
}
