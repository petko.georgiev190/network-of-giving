FROM openjdk:latest
VOLUME /tmp
ADD network-of-giving-java/target/*.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]