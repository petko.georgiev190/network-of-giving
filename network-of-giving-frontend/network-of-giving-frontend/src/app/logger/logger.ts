import { User } from '../user/userModul';
import { Charity } from '../charity/charityModul';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

export class Logger {
    eventId :number;
    user:User;
    charity:Charity;
    amountDonated:number;
    eventString: string;
    timestamp: Date;
}

// @Id
// @GeneratedValue(strategy= GenerationType.AUTO)
// @Column(name = "activity_id")
// private long activityId;

// @OneToOne
// @JoinColumn(name = "user_id")
// private User user;

// @OneToOne
// @JoinColumn(name = "charity_id")
// Charity charity;
// @Column(name="amount_donated")
// double amountDonated;
// @Column(name="activity")
// Event event;
// @Column(name="activityString")
// String activityString;
// @Column(name="time_stamp")
// Timestamp timeStamp;