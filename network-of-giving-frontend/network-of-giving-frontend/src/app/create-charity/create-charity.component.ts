import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CharityService } from '../service/data/charity.service';
import { UserService } from '../service/data/user.service';
import { first } from 'rxjs/operators';
import { User } from '../user/userModul';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-create-charity',
  templateUrl: './create-charity.component.html',
  styleUrls: ['./create-charity.component.css']
})
export class CreateCharityComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  flagNotGood =false;
  creator : string;
  constructor(
    private appComponent: AppComponent,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private charityService: CharityService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    if(!this.userService.isLoggedIn()){
      this.router.navigate([''])
    }
    this.creator=this.appComponent.user.username;
    this.form = this.formBuilder.group({
      charityName: ['', [Validators.required]],
      description: ['' ],
      budgetRequired: ['' ],
      numberOfParticipantsWanted: ['',[Validators.pattern("^[0-9]*$")]],
      thumbnailUrl: ['',[Validators.required]]
    
  })


}

get f() { return this.form.controls; }

onSubmit() {
  this.submitted = true;

  this.flagNotGood=false;
  if (this.form.invalid) {
    this.flagNotGood=true;
      return;
  }
  if(this.form.value.numberOfParticipantsWanted=='' && this.form.value.budgetRequired==''){
    this.flagNotGood=true;
    return;
  }
  this.loading = true;
  this.charityService.createCharity(this.creator,this.form.value)
  .pipe(first())
      .subscribe(
          data => {
            this.router.navigate(['']);
          },
          error => {
            this.flagNotGood=true;
              this.loading = false;
          });
}

}
