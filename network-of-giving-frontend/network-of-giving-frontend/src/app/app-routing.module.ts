import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CharityComponent } from './charity/charity.component';
import { CreateCharityComponent } from './create-charity/create-charity.component';
import { UserComponent } from './user/user.component';
import { EditCharityComponent } from './edit-charity/edit-charity.component';


const routes: Routes = [
  {path:'',component:HomePageComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'charities/:id',component:CharityComponent},
  {path:'charity-creation',component:CreateCharityComponent},
  {path:'charityEdit/:id',component:EditCharityComponent},
  {path:'user',component:UserComponent},
  {path:'**',component:HomePageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
