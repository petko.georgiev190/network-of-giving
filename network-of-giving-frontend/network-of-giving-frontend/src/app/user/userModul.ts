export class User {
    userId: number;
    username: string;
    password: string;
    name: string;
    sex: string;
    age: number;
    location: string;
    
    token: string;
}

