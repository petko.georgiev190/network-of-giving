import { Component, OnInit } from '@angular/core';
import { DonationService } from '../service/data/donation.service';
import { User } from './userModul';
import { UserService } from '../service/data/user.service';
import { Router } from '@angular/router';
import { Donation } from '../donation/donation';
import { Participation } from '../participation/participationModul';
import { ParticipationService } from '../service/data/participation.service';
import { Charity } from '../charity/charityModul';
import { CharityService } from '../service/data/charity.service';
import { LoggerService } from '../service/data/logger.service';
import { Logger } from '../logger/logger';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  isLogged=false;
  user: User;
  userId: number;
  donations :Charity[];
  participation: Charity[];
  charitiesCreated: Charity[];
  loggs: Logger[];

  constructor(
    private donationService:DonationService,
    private userService: UserService,
    private router:Router,
    private participationService:ParticipationService,
    private charityService: CharityService,
    private loggerService: LoggerService
  ) { }

  routeTo(id :number){
    this.router.navigateByUrl(`/charities/${id}`);
  }

  ngOnInit(): void {
   this.refresh();
  }
  refresh(){
    if(!this.userService.isLoggedIn()){
      this.router.navigate([''])
    }
    this.isLogged=this.userService.isLoggedIn();
    if(this.isLogged){
      this.user=JSON.parse(localStorage.getItem('user'));
      this.userId=this.user.userId;
    }


    this.charityService.retrieveCharitiesByDonator(this.userId).pipe(first()).subscribe(
      response => {
        this.donations = response;
      }
    )

    this.charityService.retrievePaticipationByParticipator(this.userId).pipe(first()).subscribe(
      response => {
        this.participation = response;
      }
    )

    this.charityService.retrieveCharitiesByCreator(this.user.username).pipe(first()).subscribe(
      response => {
        this.charitiesCreated = response;
      }
    )
    
    this.loggerService.retrieveAllByUsername(this.user.username).pipe(first()).subscribe(
      response=>{
        this.loggs=response;
      }
    )
    
  }

}
