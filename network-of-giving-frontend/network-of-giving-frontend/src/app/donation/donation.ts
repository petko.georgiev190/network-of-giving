
export class Donation {
    id:DonationKey;
    amountDonated: number;

}

export class DonationKey{
    userId: number;
    charityId:number;
}