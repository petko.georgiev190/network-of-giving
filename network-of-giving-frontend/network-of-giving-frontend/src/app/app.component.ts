import { Component } from '@angular/core';
import { User } from './user/userModul';
import { UserService } from './service/data/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'network-of-giving-frontend';
  user: User;

  constructor(private accountService: UserService) {
      this.accountService.user.subscribe(x => this.user = x);
  }

  logout() {

    if(confirm("Logout?")){
      this.accountService.logout();
    }
      
  }
}
