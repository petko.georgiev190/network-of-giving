import { User } from '../user/userModul';

export class Charity {
    charityId: number;
    charityName: string;
    description: string;
    numberOfParticipants: number;
    numberOfParticipantsWanted: number;
    budgetRequired: number;
    budgetCollected: number;
    creator: User;
    thumbnailUrl: string;
    estimatedAmount: number;
}