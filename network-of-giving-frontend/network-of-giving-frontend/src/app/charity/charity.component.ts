import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CharityService } from '../service/data/charity.service';
import { Charity } from './charityModul';
import { AppComponent } from '../app.component';
import { first } from 'rxjs/operators';
import { UserService } from '../service/data/user.service';
import { DonationService } from '../service/data/donation.service';
import { User } from '../user/userModul';
import { ParticipationService } from '../service/data/participation.service';
import { HomePageComponent } from '../home-page/home-page.component';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Donation } from '../donation/donation';

@Component({
  selector: 'app-charity',
  templateUrl: './charity.component.html',
  styleUrls: ['./charity.component.css']
})


export class CharityComponent implements OnInit {
  user :User;
  userId:number;
  id :number;
  charity : Charity;
  donationAmount: number;
  isLogged = false;
  isCreator=false;
  image :SafeStyle;
  donations: Donation[];
  sum =0;


  constructor(
    private currApp : AppComponent,
    private donationService: DonationService,
    private participationService: ParticipationService,
    private userService: UserService,
    private charityService: CharityService,
    private route: ActivatedRoute,
    private router: Router,
    private sani: DomSanitizer
  ) { }

    donation(){

      if(confirm(`Are you sure? You are about to donate ${this.donationAmount} to ${this.charity.charityName}.`)){
       this.donationService.createDonation(this.userId,this.id,this.donationAmount)
       .pipe(first())
       .subscribe(
           data => {
             this.refresh();
           },
           error => {
           });
     }
    }

    getSanitizedUrl(url) {
      return this.sani.bypassSecurityTrustResourceUrl(url);
    }

    participate(){
      if(confirm(`Are you sure? You are about to participate in ${this.charity.charityName}.`)){
   (this.user);
     this.participationService.createParticipation(this.userId,this.id)
     .pipe(first())
     .subscribe
     (
         data => {
           this.refresh();
         },
         error => {
         });
        }
    }

    delete(){
      if(confirm(`Are you sure? You are about to delete ${this.charity.charityName}.`)){
     this.charityService.deleteCharity(this.userId,this.id)
     .pipe(first())
     .subscribe(
         data => {
           this.router.navigate(['']);
         },
         error => {
         });
        }
    }
      
    routeTo(id :number){
      this.router.navigateByUrl(`/charityEdit/${id}`);
    }

  refresh(){

    this.isLogged=this.userService.isLoggedIn();
    if(this.isLogged){
      this.user=JSON.parse(localStorage.getItem('user'));
      this.userId=this.user.userId;
    }
    this.id = this.route.snapshot.params['id'];

    
    this.charityService.retrieveCharityById(this.id)
    .pipe(first()).subscribe(

      resource =>{
        this.charity=resource;
        this.isCreator=this.userId==this.charity.creator.userId;
        if(!this.charity){
          this.router.navigate([''])
        }
        this.image = this.sani.bypassSecurityTrustUrl(this.charity.thumbnailUrl);
        this.getEstimatedAmount();
      }
    )


  }

  getEstimatedAmount(){
      this.donationService.getDonationsFromUser(this.userId).pipe(first()).subscribe(
        resource =>{
          this.donations=resource;
          if(this.donations.length==0 || this.charity.budgetRequired==0){
            this.charity.estimatedAmount=0;
          }else{
            this.donations.forEach((element) =>{
              this.sum=this.sum+element.amountDonated;
            })
            this.charity.estimatedAmount=(this.sum/this.donations.length)%this.charity.budgetRequired |0;
          }
          this.sum=0;
        }
      )
      
  }


  ngOnInit(): void {

    this.refresh();

  }



}
