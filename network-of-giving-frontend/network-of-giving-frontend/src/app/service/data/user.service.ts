import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/constants';
import { User } from 'src/app/user/userModul';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user =this.userSubject.asObservable();

  }
  public isLoggedIn(){
      if(this.userSubject.value){
        return true;
      }
      return false;
  }
  public get userValue(): User{
    return this.userSubject.value;
  }

  login(username: string,password: string){
   let data = {username : username ,password :password};
    return this.http.post<User>(`${API_URL}/users/authenticate`,data)
    .pipe(map(user =>{
      localStorage.setItem('user',JSON.stringify(user));
      this.userSubject.next(user);
      return user;
    }))
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['']);
}

register(user: User) {
  return this.http.post(`${API_URL}/users`, user);
}

getAll() {
  return this.http.get<User[]>(`${API_URL}/users`);
}

getById(id: number) {
  return this.http.get<User>(`${API_URL}/users/${id}`);
}

}
