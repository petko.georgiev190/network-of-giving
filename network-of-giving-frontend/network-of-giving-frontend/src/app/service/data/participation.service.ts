import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/constants';
import { Participation } from 'src/app/participation/participationModul';

@Injectable({
  providedIn: 'root'
})
export class ParticipationService {

  constructor(
    private http:HttpClient
  ) { }

  createParticipation(userId:number, charityId:number){

    return this.http.post(`${API_URL}/users/${userId}/participatesIn/${charityId}`,null)
  }

}
