import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Logger } from 'src/app/logger/logger';
import { API_URL } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(
    private http:HttpClient
  ) { }

  
  getAll() {
    return this.http.get<Logger[]>(`${API_URL}/logger`);
  }

  retrieveAllByUsername(username:string){
    return this.http.get<Logger[]>(`${API_URL}/logger/${username}`)
  }
}
