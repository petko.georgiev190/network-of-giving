import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from 'src/app/constants';
import { Donation } from 'src/app/donation/donation';
import { Charity } from 'src/app/charity/charityModul';

@Injectable({
  providedIn: 'root'
})
export class DonationService {

  constructor(
    private http:HttpClient
  ) { }


  createDonation(userId:number, charityId:number,amountDonated:number){
    amount : Number;
    return this.http.put(`${API_URL}/users/${userId}/donatesTo/${charityId}/donate/${amountDonated}`,null)
  }

  getDonationsFromUser(userId:number){
    return this.http.get<Donation[]>(`${API_URL}/donations/from/${userId}`)
  }
}
