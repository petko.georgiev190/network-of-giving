import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Charity } from 'src/app/charity/charityModul';
import { API_URL } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class CharityService {

  constructor(
    private http:HttpClient
  ) {
   }

   

   retrieveAllCharities(){
     return this.http.get<Charity[]>(`${API_URL}/charities`)
   }

   retrieveCharityById(id :number){
    return this.http.get<Charity>(`${API_URL}/charities/getById/${id}`)
  }

  retrieveCharityByName(charityName :string){
    return this.http.get<Charity>(`${API_URL}/getByName/${charityName}`)
  }

  searchForCharityByName(charityName :string){
    return this.http.get<Charity[]>(`${API_URL}/charities/searchByName/${charityName}`)
  }

  retrieveCharitiesByCreator(username :string){
    return this.http.get<Charity[]>(`${API_URL}/charities/getByCreator/${username}`)
  }

  createCharity(username:string, charity:Charity){
    return this.http.post(`${API_URL}/users/${username}/charities/create`,charity)
  }
  
  updateCharity(username:string,charityId:number, charity:Charity){
    return this.http.put(`${API_URL}/users/${username}/charities/${charityId}`,charity)
  }
  deleteCharity(userId:number,charityId:number){
    return this.http.delete(`${API_URL}/charities/delete/${userId}/${charityId}`)
  }

  retrieveCharitiesByDonator(userId:number){
    return this.http.get<Charity[]>(`${API_URL}/donations/from/${userId}/to`)
  }

  retrievePaticipationByParticipator(userId:number){
    return this.http.get<Charity[]>(`${API_URL}/participation/from/${userId}/to`)
  }



  

}
