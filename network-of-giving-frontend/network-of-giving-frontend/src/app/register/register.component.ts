import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/data/user.service';
import { first } from 'rxjs/operators';
import { User } from '../user/userModul';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  flagNotGood =false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: UserService
  ) {
    if (this.accountService.userValue) {
      this.router.navigate(['/']);
  }
   }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      sex: ['', Validators.required],
      age: ['',Validators.required,Validators.pattern("^[0-9]*$")],
      location: ['',Validators.required]
      
    //   id: number;
    // username: string;
    // password: string;
    // name: string;
    // sex: string;
    // age: number;
    // location: string;
    
  });
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;

    this.flagNotGood=false;
    // stop here if form is invalid
    if (this.form.invalid) {
      this.flagNotGood=true;
        return;
    }

    this.loading = true;
    this.accountService.register(this.form.value)
        .pipe(first())
        .subscribe(
            data => {
                this.router.navigate(['../login'], { relativeTo: this.route });
            },
            error => {
              this.flagNotGood=true;
                this.loading = false;
            });
}

}
