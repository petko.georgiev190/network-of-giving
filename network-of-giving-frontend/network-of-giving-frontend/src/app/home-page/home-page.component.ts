import { Component, OnInit } from '@angular/core';
import { CharityService } from '../service/data/charity.service';
import { Charity } from '../charity/charityModul';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  charities : Charity[];
  message: string;
  searchBy: string;
  
  constructor(private charityService: CharityService,private router:Router) { }
  
  ngOnInit(): void {
    this.refreshCharities();
  }
  submitFilter(){
    if(this.searchBy){
    this.charityService.searchForCharityByName(this.searchBy).pipe(first()).subscribe(
      response =>{
        this.charities=response;
        
      }
    )
    }
  }

  routeTo(id :number){
    this.router.navigateByUrl(`/charities/${id}`);
  }


  refreshCharities(){
    this.charityService.retrieveAllCharities()
      .pipe(first()).subscribe(
      response => {
        this.charities = response;
      }
    )
  }
}
