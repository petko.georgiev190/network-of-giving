import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/data/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CharityService } from '../service/data/charity.service';
import { first } from 'rxjs/operators';
import { Charity } from '../charity/charityModul';

@Component({
  selector: 'app-edit-charity',
  templateUrl: './edit-charity.component.html',
  styleUrls: ['./edit-charity.component.css']
})
export class EditCharityComponent implements OnInit {
  id :number;
  form: FormGroup;
  loading = false;
  submitted = false;
  flagNotGood =false;
  creator : string;
  charity :Charity;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private appComponent: AppComponent,
    private charityService: CharityService
  ) {
    

  }

  ngOnInit(): void {

    this.id = this.route.snapshot.params['id'];
    if(!this.userService.isLoggedIn()){
      this.router.navigate([''])
    }
    this.creator=this.appComponent.user.username;
    this.form = this.formBuilder.group({
      charityName: ['' ],
      description: ['' ],
      budgetRequired: ['' ],
      numberOfParticipantsWanted: ['',[Validators.pattern("^[0-9]*$")]],
      thumbnailUrl: ['',]
    
  })
  this.charityService.retrieveCharityById(this.id)
  .pipe(first()).subscribe(

    resource =>{
      this.charity=resource;

    }
  )
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;
  
    this.flagNotGood=false;
    if (this.form.invalid) {
      this.flagNotGood=true;
        return;
    }

    this.loading = true;
    this.charityService.updateCharity(this.creator,this.id,this.form.value)
    .pipe(first())
        .subscribe(
            data => {
              this.router.navigate([`charities/${this.id}`]);
            },
            error => {
              this.flagNotGood=true;
                this.loading = false;
            });
  }

}
